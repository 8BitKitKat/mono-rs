﻿using System;
using System.Runtime.InteropServices;

namespace HelloWorldDotnet
{
    class Program
    {
        [DllImport ("__Internal")]
        public static extern void RustFunction();

        [DllImport ("__Internal")]
        public static extern void RustGreet(string name);

        [DllImport ("__Internal")]
        public static extern int RustAnswer();

        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!, From c#");

            RustFunction();
            RustGreet("MONO!");
            Console.WriteLine(RustAnswer());

            foreach(var arg in args)
            {
                Console.WriteLine(arg);
            }

            // var c = new ClassWrapper(5, 6);
            // ClassWrapper.PassByPtrIn(c);
            // ClassWrapper.PassByPtrOut(c);
            // ClassWrapper.PassByPtrInOut(c);
            // ClassWrapper.PassByDoubleIndirection(ref c);

            // var c2 = ClassWrapper.ReturnByPtr(); // sigabrt

			// var s = new StructWrapper(7, 8);
			// StructWrapper.PassByValue(s);
			// StructWrapper.PassByPtrIn(ref s);
			// StructWrapper.PassByPtrOut(out s);
			// StructWrapper.PassByPtrInOut(ref s);
            // s.PassByValue2();
            
            // var s2 = StructWrapper.ReturnByValue(); 
            // Console.WriteLine(s2.x);
			// Console.WriteLine(s2.y);
			// var s3 = StructWrapper.ReturnByPtrCLS();

            var vec2 = new Vec2(5.3f, 6.9f);
            vec2.DebugPrint();
            vec2 += new Vec2(0.4f, 13.2f);
            vec2.DebugPrint();

            var ent = new Entity(vec2);
            ent.Translate(new Vec2(54.6f, 27.0f));
            ent.DebugPrint();

            var p = new Player();
            p.DebugPrint();
            p.Translate(new Vec2(83.7f, 72.7f));
            Console.WriteLine($"Player pos: {p.pos.x}, {p.pos.y}");
            
            var vec3 = new Vec3(8.4f, 15.2f, 89.4f);
            vec3.DebugPrint();
        }
    }

    // Note: Sequential layout
    [StructLayout (LayoutKind.Sequential)]
    class ClassWrapper
    {
        public int x;
        public int y;

        public ClassWrapper(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        // Cannot wrap PassByValue

        [DllImport ("__Internal")]
        public static extern void PassByPtrIn(ClassWrapper s);

        [DllImport ("__Internal")]
        public static extern void PassByPtrOut([Out] ClassWrapper s);

        [DllImport ("__Internal")]
        public static extern void PassByPtrInOut([In, Out] ClassWrapper s);

        // Cannot wrap Return by value

        [DllImport ("__Internal")]
        public static extern ClassWrapper ReturnByPtr();

        [DllImport ("__Internal")]
        public static extern void PassByDoubleIndirection(ref ClassWrapper s);
    }

    struct StructWrapper
    {
        public int x;
        public int y;

        public StructWrapper(int x, int y)
        {
        	this.x = x;
        	this.y = y;
        }

        [DllImport ("__Internal")]
        public static extern void PassByValue(StructWrapper s);

        public void PassByValue2()
        {
            PassByValue(this);
        }

        [DllImport ("__Internal")]
        public static extern void PassByPtrIn(ref StructWrapper s);

        [DllImport ("__Internal")]
        public static extern void PassByPtrOut(out StructWrapper s);

        [DllImport ("__Internal")]
        public static extern void PassByPtrInOut(ref StructWrapper s);

        [DllImport ("__Internal")]
        public static extern StructWrapper ReturnByValue();

        // Safe, CLS-compliant, way
        [DllImport ("__Internal", EntryPoint="ReturnByPtr")]
        public static extern IntPtr ReturnByPtrCLS();

         // // "unsafe" way
         // [DllImport ("__Internal", EntryPoint="ReturnByPtr")]
         // public static unsafe extern StructWrapper* ReturnByPtrUnsafe();
    }

    struct Vec2
    {
    	public float x;
    	public float y;
    
        public Vec2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
        
        [DllImport("__Internal", EntryPoint = "vec2_debug_print")]
        private static extern void ExternDebugPrint(ref Vec2 s);
        
        public void DebugPrint()
        {
            ExternDebugPrint(ref this);
        }

        public static Vec2 operator +(Vec2 a, Vec2 b)
            => new Vec2(a.x + b.x, a.y + b.y);
    }

    struct Vec3
    {
        public float x;
        public float y;
        public float z;

        public Vec3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        [DllImport("__Internal", EntryPoint="vec3_debug_print")]
        private static extern void ExternDebugPrint(ref Vec3 s);

        public void DebugPrint()
        {
            ExternDebugPrint(ref this);
        }

        public static Vec3 operator +(Vec3 a, Vec3 b)
            => new Vec3(a.x + b.x, a.y + b.y, a.z = b.z);
    }

    [StructLayout (LayoutKind.Sequential)] // needed to pass `self` to rust
    class Entity
    {
        public Vec2 pos;

        public Entity()
        {
            pos = new Vec2(0.0f, 0.0f);
        }

        public Entity(Vec2 pos)
        {
            this.pos = pos;
        }

        [DllImport("__Internal", EntryPoint = "entity_debug_print")]
        private static extern void ExternDebugPrint(Entity s); // ref not needed for classes

        public void DebugPrint()
        {
            ExternDebugPrint(this);
        }

        public void Translate(Vec2 to)
        {
            pos += to;
        }
    }

    [StructLayout (LayoutKind.Sequential)]
    class Player : Entity
    {
        [DllImport("__Internal", EntryPoint = "player_debug_print")]
        private static extern void ExternDebugPrint(Player s);

        public new void DebugPrint()
        {
            ExternDebugPrint(this);
        }
    }
}
