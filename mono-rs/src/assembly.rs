use crate::{c_string_ptr, domain::Domain, image::Image, mono_sys};
use std::os::raw::{c_char, c_int, c_void};

// Mono Assembly
#[derive(Debug, Clone)]
pub struct Assembly {
    filename: String,
    assembly: *mut mono_sys::MonoAssembly,
    aname: *mut mono_sys::MonoAssemblyName,
    name: *const c_char,
    image: Image,
}

impl Assembly {
    /// Create a new assembly.
    ///
    /// # Panics?
    ///
    /// Mono will crash if this is run and the path is not a correct mono executable,
    /// halting the whole program
    pub fn new<S: Into<String>>(domain: &Domain, path: S) -> Self {
        let filename = path.into();
        let assembly = unsafe {
            mono_sys::mono_domain_assembly_open(
                domain.get_inner_domain(),
                c_string_ptr!(filename.clone()),
            )
        };

        let aname = unsafe { mono_sys::mono_assembly_get_name(assembly) };
        let image = unsafe { Image::new(mono_sys::mono_assembly_get_image(assembly)) };
        let name = unsafe { mono_sys::mono_assembly_name_get_name(aname) };

        Self {
            filename,
            assembly,
            aname,
            name,
            image,
        }
    }

    /// Get the stored assembly filename
    pub fn filename(&self) -> &String {
        &self.filename
    }

    /// Get the raw `*mut mono_sys::MonoAssembly` object.
    pub unsafe fn inner_assembly(&self) -> *mut mono_sys::MonoAssembly {
        self.assembly
    }

    /// Get the raw `*mut mono_sys::MonoAssemblyName` object.
    pub unsafe fn inner_aname(&self) -> *mut mono_sys::MonoAssemblyName {
        self.aname
    }

    /// Get the raw `*mut mono_sys::MonoImage` object.
    pub unsafe fn inner_image(&self) -> *mut mono_sys::MonoImage {
        self.image.inner_image()
    }
}

impl std::cmp::PartialEq for Assembly {
    fn eq(&self, other: &Self) -> bool {
        match unsafe { self.mono_assembly_names_equal(other.inner_aname()) } {
            0 => false,
            _ => true,
        }
    }
}

impl std::cmp::Eq for Assembly {}

/// # Assembly Loading
///
/// [C docs](http://docs.go-mono.com/?link=xhtml%3adeploy%2fmono-api-assembly.html)
impl Assembly {
    /// # Description
    ///
    /// This method releases a reference to the assembly. The assembly is only released when
    /// all the outstanding references to it are released.
    pub unsafe fn mono_assembly_close(&self) {
        mono_sys::mono_assembly_close(self.assembly);
    }

    pub unsafe fn mono_assembly_get_object(
        &self,
        domain: &Domain,
    ) -> *mut mono_sys::MonoReflectionAssembly {
        mono_sys::mono_assembly_get_object(domain.get_inner_domain(), self.assembly)
    }

    /// # Description
    ///
    /// Loads the assembly referenced by aname, if the value of basedir is not `NULL`,
    /// it attempts to load the assembly from that directory before probing the standard locations.
    ///
    /// # Return value
    ///
    /// The assembly referenced by aname loaded or `NULL` on error. On error the value pointed
    /// by status is updated with an error code.
    ///
    /// # Arguments
    ///
    /// * `aname` - A MonoAssemblyName with the assembly name to load.
    /// * `basedir` - A directory to look up the assembly at.
    /// * `status` - a pointer to a MonoImageOpenStatus to return the status of the load operation
    pub unsafe fn mono_assembly_load(
        aname: *mut mono_sys::MonoAssemblyName,
        basedir: *const c_char,
        status: *mut mono_sys::MonoImageOpenStatus,
    ) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_assembly_load(aname, basedir, status)
    }

    /// # Description
    ///
    /// Loads the assembly referenced by aname, if the value of basedir is not `NULL`, it attempts
    /// to load the assembly from that directory before probing the standard locations.<br/>
    /// If the assembly is being opened in reflection-only mode (refonly set to `TRUE`) then no
    /// assembly binding takes place.
    ///
    /// # Return value
    ///
    /// The assembly referenced by aname loaded or NULL on error. On error the value pointed by
    /// status is updated with an error code.
    ///
    /// # Parameters
    ///
    /// * aname - A MonoAssemblyName with the assembly name to load.
    /// * basedir - A directory to look up the assembly at.
    /// * status - A pointer to a MonoImageOpenStatus to return the status of the load operation.
    /// * refonly - Whether this assembly is being opened in "reflection-only" mode.
    pub unsafe fn mono_assembly_load_full(
        aname: *mut mono_sys::MonoAssemblyName,
        basedir: *const c_char,
        status: *mut mono_sys::MonoImageOpenStatus,
        refonly: mono_sys::mono_bool,
    ) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_assembly_load_full(aname, basedir, status, refonly)
    }

    /// # Description
    ///
    /// This is used to determine if the specified assembly has been loaded.
    ///
    /// # Return value
    ///
    /// `NULL` If the given aname assembly has not been loaded, or a pointer to a `MonoAssembly`
    /// that matches the `MonoAssemblyName` specified.
    ///
    /// # Parameters
    ///
    /// * aname - An assembly to look for.
    pub unsafe fn mono_assembly_loaded(
        aname: *mut mono_sys::MonoAssemblyName,
    ) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_assembly_loaded(aname)
    }

    /// # Description
    ///
    /// If the provided image has an assembly reference, it will process the given image as an
    /// assembly with the given name.<br/>
    /// Most likely you want to use the `api:mono_assembly_load_full` method instead.<br/>
    /// This is equivalent to calling `api:mono_assembly_load_from_full` with the refonly parameter
    /// set to `FALSE`.
    ///
    /// # Return value
    ///
    /// A valid pointer to a `MonoAssembly*` on success and the status will be set to
    /// `MONO_IMAGE_OK`; or `NULL` on error.<br/>
    /// If there is an error loading the assembly the status will indicate the reason with status
    /// being set to `MONO_IMAGE_INVALID` if the image did not contain an assembly reference table.
    ///
    /// # Parameters
    ///
    /// * image - Image to load the assembly from.
    /// * fname - Assembly name to associate with the assembly.
    /// * status - Return status code.
    pub unsafe fn mono_assembly_load_from(
        image: *mut mono_sys::MonoImage,
        fname: *const c_char,
        status: *mut mono_sys::MonoImageOpenStatus,
    ) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_assembly_load_from(image, fname, status)
    }

    /// # Description
    ///
    /// If the provided image has an assembly reference, it will process the given image as an
    /// assembly with the given name.<br/>
    /// Most likely you want to use the `api:mono_assembly_load_full` method instead.
    ///
    /// # Return value
    ///
    /// A valid pointer to a `MonoAssembly*` on success and the status will be set to
    /// `MONO_IMAGE_OK`; or NULL on error.<br/>
    /// If there is an error loading the assembly the status will indicate the reason with status
    /// being set to `MONO_IMAGE_INVALID` if the image did not contain an assembly reference table.
    ///
    /// #  Parameters
    ///
    /// * image - Image to load the assembly from
    /// * fname - assembly name to associate with the assembly
    /// * status - returns the status condition
    /// * refonly - Whether this assembly is being opened in "reflection-only" mode.
    pub unsafe fn mono_assembly_load_from_full(
        image: *mut mono_sys::MonoImage,
        fname: *const c_char,
        status: *mut mono_sys::MonoImageOpenStatus,
        refonly: mono_sys::mono_bool,
    ) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_assembly_load_from_full(image, fname, status, refonly)
    }

    /// # Description
    ///
    /// Loads a Mono Assembly from a name. The name is parsed using `api:mono_assembly_name_parse`,
    /// so it might contain a qualified type name, version, culture and token.<br/>
    /// This will load the assembly from the file whose name is derived from the assembly name by
    /// appending the .dll extension.<br/>
    /// The assembly is loaded from either one of the extra Global Assembly Caches specified by the
    /// extra GAC paths (specified by the `MONO_GAC_PREFIX` environment variable) or if that fails
    /// from the GAC.
    ///
    /// # Return value
    ///
    /// `NULL` on failure, or a pointer to a `MonoAssembly` on success.
    ///
    /// # Parameters
    ///
    /// * name - an assembly name that is then parsed by api:mono_assembly_name_parse.
    /// * status - return status code
    pub unsafe fn mono_assembly_load_with_partial_name(
        name: *const c_char,
        status: *mut mono_sys::MonoImageOpenStatus,
    ) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_assembly_load_with_partial_name(name, status)
    }

    /// # Description
    ///
    /// This loads an assembly from the specified filename. The filename allows a local
    /// URL (starting with a file:// prefix). If a file prefix is used, the filename is
    /// interpreted as a URL, and the filename is URL-decoded. Otherwise the file is treated
    /// as a local path.<br/>
    /// First, an attempt is made to load the assembly from the bundled executable
    /// (for those deployments that have been done with the `mkbundle` tool or for scenarios where
    /// the assembly has been registered as an embedded assembly). If this is not the case, then
    /// the assembly is loaded from disk using `api:mono_image_open_full`.<br/>
    /// If the pointed assembly does not live in the Global Assembly Cache, a shadow copy of the
    /// assembly is made.<br/>
    ///
    /// # Return value
    ///
    /// a pointer to the MonoAssembly if filename contains a valid assembly or `NULL` on error.
    /// Details about the error are stored in the status variable.
    ///
    /// # Parameters
    ///
    /// * filename - Opens the assembly pointed out by this name
    /// * status - return status code
    pub unsafe fn mono_assembly_open(
        filename: *const c_char,
        status: *mut mono_sys::MonoImageOpenStatus,
    ) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_assembly_open(filename, status)
    }

    pub unsafe fn mono_assembly_open_full(
        filename: *const c_char,
        status: *mut mono_sys::MonoImageOpenStatus,
        refonly: mono_sys::mono_bool,
    ) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_assembly_open_full(filename, status, refonly)
    }

    /// # Description
    ///
    /// Use this method to override the standard assembly lookup system and override any assemblies
    /// coming from the `GAC`. This is the method that supports the `MONO_PATH` variable.<br/>
    /// Notice that `MONO_PATH` and this method are really a very bad idea as it prevents the
    /// `GAC` from working and it prevents the standard resolution mechanisms from working.
    /// Nonetheless, for some debugging situations and bootstrapping setups, this is useful to have.
    ///
    /// # Parameters
    ///
    /// * path - list of paths that contain directories where Mono will look for assemblies
    pub unsafe fn mono_set_assemblies_path(path: *const c_char) {
        mono_sys::mono_set_assemblies_path(path);
    }

    /// # Description
    ///
    /// Registers the root directory for the Mono runtime, for Linux and Solaris 10,
    /// this auto-detects the prefix where Mono was installed.
    pub unsafe fn mono_set_rootdir() {
        mono_sys::mono_set_rootdir();
    }
}

/// # Working with assemblies
///
/// Although some of these don't require `self`, im including it since it could
/// result in undefined behavior if we try to call these without any assemblies loaded.
///
/// [C docs](http://docs.go-mono.com/?link=xhtml%3adeploy%2fmono-api-assembly.html)
impl Assembly {
    pub unsafe fn mono_assembly_fill_assembly_name(
        &self,
        image: *mut mono_sys::MonoImage,
        aname: *mut mono_sys::MonoAssemblyName,
    ) -> mono_sys::mono_bool {
        mono_sys::mono_assembly_fill_assembly_name(image, aname)
    }

    /// # Description
    ///
    /// Invokes the provided func callback for each assembly loaded into the runtime.
    /// The first parameter passed to the callback is the `MonoAssembly*`, and the second parameter
    /// is the user_data.<br/>
    /// This is done for all assemblies loaded in the runtime, not just those loaded in the
    /// current application domain.
    ///
    /// # Parameters
    ///
    /// * func - function to invoke for each assembly loaded
    /// * user_data - data passed to the callback
    pub unsafe fn mono_assembly_foreach(
        &self,
        func: mono_sys::MonoFunc,
        user_data: *mut ::std::os::raw::c_void,
    ) {
        mono_sys::mono_assembly_foreach(func, user_data);
    }

    /// # Return value
    ///
    /// the MonoImage associated with this assembly.
    pub unsafe fn mono_assembly_get_image(&self) -> *mut mono_sys::MonoImage {
        mono_sys::mono_assembly_get_image(self.assembly)
    }

    /// # Return value
    ///
    /// the assembly for the application, the first assembly that is loaded by the VM.
    pub unsafe fn mono_assembly_get_main(&self) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_assembly_get_main()
    }

    /// # Description
    ///
    /// The returned name's lifetime is the same as assembly's.<br/>
    /// *c-lifetime.
    ///
    /// # Return value
    ///
    /// the MonoAssemblyName associated with this assembly.
    pub unsafe fn mono_assembly_get_name(&self) -> *mut mono_sys::MonoAssemblyName {
        mono_sys::mono_assembly_get_name(self.assembly)
    }

    /// # Description
    ///
    /// Obtains the root directory used for looking up assemblies.
    ///
    /// # Return value
    ///
    /// a string with the directory, this string should not be freed.
    pub unsafe fn mono_assembly_get_rootdir(&self) -> *const c_char {
        mono_sys::mono_assembly_getrootdir()
    }

    /// # Description
    ///
    /// Fills out the aname with the assembly name of the index assembly reference in image.
    ///
    /// # Parameters
    ///
    /// * index - index to the assembly reference in the image.
    pub unsafe fn mono_assembly_get_assembly_ref(&self, index: c_int) {
        mono_sys::mono_assembly_get_assemblyref(self.image.inner_image(), index, self.aname);
    }

    /// # Description
    ///
    /// This is used to determine if the specified assembly has been loaded.
    ///
    /// # Return value
    ///
    /// `NULL` If the given aname assembly has not been loaded, or a pointer to a MonoAssembly that
    /// matches the MonoAssemblyName specified.
    ///
    /// # Parameters
    ///
    /// * refonly - Whether this assembly is being opened in "reflection-only" mode.
    pub unsafe fn mono_assembly_loaded_full(
        &self,
        refonly: mono_sys::mono_bool,
    ) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_assembly_loaded_full(self.aname, refonly)
    }

    pub unsafe fn mono_assembly_load_reference(&self, index: c_int) {
        mono_sys::mono_assembly_load_reference(self.image.inner_image(), index);
    }

    /// # Description
    ///
    /// This method is now a no-op, it does nothing other than setting the status to `MONO_IMAGE_OK`
    #[deprecated(note = "There is no reason to use this method anymore, it does nothing")]
    pub unsafe fn mono_assembly_load_references(&self, status: *mut mono_sys::MonoImageOpenStatus) {
        mono_sys::mono_assembly_load_references(self.image.inner_image(), status);
    }

    pub unsafe fn mono_assembly_load_module(&self, idx: u32) -> *mut mono_sys::MonoImage {
        mono_sys::mono_assembly_load_module(self.assembly, idx)
    }

    pub unsafe fn mono_assembly_invoke_load_hook(&self) {
        mono_sys::mono_assembly_invoke_load_hook(self.assembly);
    }

    pub unsafe fn mono_assembly_invoke_search_hook(&self) {
        mono_sys::mono_assembly_invoke_search_hook(self.aname);
    }

    pub unsafe fn mono_assembly_set_main(&self) {
        mono_sys::mono_assembly_set_main(self.assembly);
    }

    /// # Description
    ///
    /// This routine sets the internal default root directory for looking up assemblies.<br/>
    /// This is used by Windows installations to compute dynamically the place where the Mono
    /// assemblies are located.
    ///
    /// # Parameters
    ///
    /// * root_dir - The pathname of the root directory where we will locate assemblies
    pub unsafe fn mono_assembly_set_rootdir(&self, root_dir: *const c_char) {
        mono_sys::mono_assembly_setrootdir(root_dir);
    }

    pub unsafe fn mono_register_config_for_assembly(
        &self,
        assembly_name: *const c_char,
        config_xml: *const c_char,
    ) {
        mono_sys::mono_register_config_for_assembly(assembly_name, config_xml);
    }

    pub unsafe fn mono_register_symfile_for_assembly(
        &self,
        assembly_name: *const c_char,
        raw_contents: *const mono_sys::mono_byte,
        size: c_int,
    ) {
        mono_sys::mono_register_symfile_for_assembly(assembly_name, raw_contents, size);
    }
}

/// # Assembly Names
///
/// The MonoAssemblyName contains the full identity of an assembly
/// (name, culture, public key, public key token, version and any other flags).
/// These unmanaged objects represent the
/// [System.Reflection.AssemblyName](https://www.mono-project.com/monodoc/T:System.Reflection.AssemblyName)
/// managed type.
///
/// [C docs](http://docs.go-mono.com/?link=xhtml%3adeploy%2fmono-api-assembly.html)
impl Assembly {
    /// # Description
    ///
    /// Allocate a new MonoAssemblyName and fill its values from the passed name.
    ///
    /// # Return value
    ///
    /// a newly allocated structure or `NULL` if there was any failure
    ///
    /// # Parameters
    ///
    /// * name - name to parse
    pub unsafe fn mono_assembly_name_new(&self) -> *mut mono_sys::MonoAssemblyName {
        mono_sys::mono_assembly_name_new(self.name)
    }

    pub unsafe fn mono_assembly_name_get_name(&self) -> *const c_char {
        mono_sys::mono_assembly_name_get_name(self.aname)
    }

    pub unsafe fn mono_assembly_name_get_culture(&self) -> *const c_char {
        mono_sys::mono_assembly_name_get_culture(self.aname)
    }

    pub unsafe fn mono_assembly_name_get_version(
        &self,
        minor: *mut u16,
        build: *mut u16,
        revision: *mut u16,
    ) -> u16 {
        mono_sys::mono_assembly_name_get_version(self.aname, minor, build, revision)
    }

    pub unsafe fn mono_assembly_name_get_pub_key_token(&self) -> *mut mono_sys::mono_byte {
        mono_sys::mono_assembly_name_get_pubkeytoken(self.aname)
    }

    /// # Description
    ///
    /// Frees the provided assembly name object.
    /// (it does not frees the object itself, only the name members).
    pub unsafe fn mono_assembly_name_free(&self) {
        mono_sys::mono_assembly_name_free(self.aname);
    }

    /// # Description
    ///
    /// Convert aname into its string format. The returned string is dynamically allocated
    /// and should be freed by the caller.
    ///
    /// # Return value
    ///
    /// a newly allocated string with a string representation of the assembly name.
    pub unsafe fn mono_stringify_assembly_name(&self) -> *const c_char {
        mono_sys::mono_stringify_assembly_name(self.aname)
    }

    /// # Description
    ///
    /// Compares two MonoAssemblyNames and returns whether they are equal.<br/>
    /// This compares the names, the cultures, the release version and their public tokens.
    ///
    /// # Return value
    ///
    /// `TRUE` if both assembly names are equal.
    ///
    /// # Parameters
    ///
    /// * r - second assembly.
    pub unsafe fn mono_assembly_names_equal(
        &self,
        r: *mut mono_sys::MonoAssemblyName,
    ) -> mono_sys::mono_bool {
        mono_sys::mono_assembly_names_equal(self.aname, r)
    }
}

/// # Modules
///
/// An assembly is made up of one or more modules.
///
/// [C docs](http://docs.go-mono.com/?link=xhtml%3adeploy%2fmono-api-assembly.html)
impl Assembly {
    pub unsafe fn mono_module_file_get_object(
        &self,
        domain: *mut mono_sys::MonoDomain,
        table_index: c_int,
    ) -> *mut mono_sys::MonoReflectionModule {
        mono_sys::mono_module_file_get_object(domain, self.image.inner_image(), table_index)
    }

    pub unsafe fn mono_module_get_object(
        &self,
        domain: *mut mono_sys::MonoDomain,
    ) -> *mut mono_sys::MonoReflectionModule {
        mono_sys::mono_module_get_object(domain, self.image.inner_image())
    }
}

/// # Advanced
///
/// [C docs](http://docs.go-mono.com/?link=xhtml%3adeploy%2fmono-api-assembly.html)
impl Assembly {
    pub unsafe fn mono_install_assembly_load_hook(
        func: mono_sys::MonoAssemblyLoadFunc,
        user_data: *mut c_void,
    ) {
        mono_sys::mono_install_assembly_load_hook(func, user_data);
    }

    pub unsafe fn mono_install_assembly_search_hook(
        func: mono_sys::MonoAssemblySearchFunc,
        user_data: *mut c_void,
    ) {
        mono_sys::mono_install_assembly_search_hook(func, user_data);
    }

    pub unsafe fn mono_install_assembly_refonly_search_hook(
        func: mono_sys::MonoAssemblySearchFunc,
        user_data: *mut c_void,
    ) {
        mono_sys::mono_install_assembly_refonly_search_hook(func, user_data);
    }

    pub unsafe fn mono_install_assembly_preload_hook(
        func: mono_sys::MonoAssemblyPreLoadFunc,
        user_data: *mut c_void,
    ) {
        mono_sys::mono_install_assembly_preload_hook(func, user_data);
    }

    pub unsafe fn mono_install_assembly_refonly_preload_hook(
        func: mono_sys::MonoAssemblyPreLoadFunc,
        user_data: *mut c_void,
    ) {
        mono_sys::mono_install_assembly_refonly_preload_hook(func, user_data);
    }

    pub unsafe fn mono_install_assembly_postload_search_hook(
        func: mono_sys::MonoAssemblySearchFunc,
        user_data: *mut c_void,
    ) {
        mono_sys::mono_install_assembly_postload_search_hook(func, user_data);
    }
}
