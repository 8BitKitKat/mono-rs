pub extern crate mono_sys;

pub mod assembly;
pub mod domain;
pub mod image;

/// Parse a mono config.
///
/// If given `Some(path)` the file at that location will be loaded.
/// If given `None` the default system config will be loaded.
pub fn load_mono_config(path: Option<&str>) {
    match path {
        None => unsafe {
            mono_sys::mono_config_parse(std::ptr::null());
        },
        Some(f) => unsafe {
            mono_sys::mono_config_parse(c_string_ptr!(f.to_string()));
        },
    }
}

/// Parse a mono config from a string
pub fn load_mono_config_str(str: &str) {
    unsafe {
        mono_sys::mono_config_parse_memory(c_string_ptr!(str));
    }
}

/// Create a c string
#[macro_export]
macro_rules! c_string {
    ($str:expr) => {
        std::ffi::CString::new($str).unwrap()
    };
}

/// Create a null-terminated C string ptr from a rust str / String
#[macro_export]
macro_rules! c_string_ptr {
    ($str:expr) => {
        $crate::c_string!($str).as_ptr()
    };
}

/// Create a raw null-terminated C string form a rust str / String
#[macro_export]
macro_rules! c_string_raw {
    ($str:expr) => {
        $crate::c_string!($str).into_raw()
    };
}

/// Create a rust string from a null-terminated C string ptr
///
/// # Note
///
/// Uses unsafe block.
#[macro_export]
macro_rules! str_from_c_ptr {
    ($char_ptr:expr) => {
        unsafe { std::ffi::CStr::from_ptr($char_ptr).to_str().unwrap() }
    };
}
