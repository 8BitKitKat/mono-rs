fn main() {
    println!("cargo:rerun-if-changed=src_cSharp/HelloWorldDotnet");
    if !std::process::Command::new("dotnet")
        .arg("build")
        .arg("src_cSharp/HelloWorldDotnet/HelloWorldDotnet.csproj")
        .spawn()
        .unwrap()
        .wait()
        .unwrap()
        .success()
    {
        panic!("Failed to build dotnet");
    }
}
