#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(improper_ctypes)] // TODO Fix this

// include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
pub mod bindings;
pub use bindings::*;
