extern crate mono;

// fn get_filename() -> String {
//     let filename = std::env::args().nth(1).unwrap_or_else(|| {
//         println!("No filename given");
//         std::process::exit(2);
//     });
//
//     // Make sure file exists
//     let path = std::path::Path::new(&filename);
//     if !path.exists() {
//         println!("Invalid path");
//         std::process::exit(2);
//     } else if !path.is_file() {
//         println!("Please provide a file");
//         std::process::exit(2);
//     }
//
//     filename
// }

fn main() {
    mono_rs_bin_lib::dummy();

    mono::load_mono_config(None);

    // let filename = get_filename();
    let filename = "src_cSharp/HelloWorldDotnet/bin/Debug/netcoreapp3.1/HelloWorldDotnet.dll";

    let domain = mono::domain::Domain::new("test");
    let assembly = mono::assembly::Assembly::new(&domain, filename);

    // let _r = domain.execute_assembly(&assembly);
    let _r = domain
        .execute_assembly_with_args()
        .arg("foo")
        .arg("bar")
        .run(&assembly);
}
