use crate::{assembly::Assembly, c_string_ptr, c_string_raw, mono_sys};
use std::os::raw::{c_char, c_int, c_void};

/// Mono Domain
#[derive(Debug)]
pub struct Domain {
    domain: *mut mono_sys::MonoDomain,
    context: *mut mono_sys::MonoAppContext,
}

impl Domain {
    /// Create a new Domain.
    ///
    /// # Note:
    /// You cannot create multiple Domains, This crate does not prevent you form doing so for risk of
    /// complicating things to much. This also goes if this is dropped. In general, don't
    /// call this method twice.
    pub fn new<S: Into<String>>(name: S) -> Self {
        let domain = unsafe { mono_sys::mono_jit_init(c_string_ptr!(name.into())) };
        let context = unsafe { mono_sys::mono_context_get() };

        Self { domain, context }
    }

    /// Execute the given assembly
    pub fn execute_assembly(&self, assembly: &Assembly) -> i32 {
        unsafe {
            mono_sys::mono_jit_exec(
                self.domain,
                assembly.inner_assembly(),
                1,
                [c_string_raw!(assembly.filename().to_string())].as_mut_ptr(),
            )
        }
    }

    pub fn execute_assembly_with_args(&self) -> ArgsBuilder {
        ArgsBuilder::new(self)
    }

    fn execute_assembly_with_args_raw(
        &self,
        assembly: &Assembly,
        count: i32,
        args: *mut *mut std::os::raw::c_char,
    ) -> i32 {
        unsafe { mono_sys::mono_jit_exec(self.domain, assembly.inner_assembly(), count, args) }
    }

    /// Get the raw `*mut mono_sys::MonoDomain` object.
    ///
    /// # Unsafe
    /// You shouldn't be using this unless you know what you are doing.
    /// If you do need this you are most likely already in an unsafe block.
    pub unsafe fn get_inner_domain(&self) -> *mut mono_sys::MonoDomain {
        self.domain
    }
}

impl Drop for Domain {
    fn drop(&mut self) {
        unsafe { mono_sys::mono_jit_cleanup(self.domain) }
    }
}

#[derive(Debug)]
pub struct ArgsBuilder<'a> {
    args: Vec<*mut std::os::raw::c_char>,
    count: i32,
    domain: &'a Domain,
}

impl<'a> ArgsBuilder<'a> {
    fn new(domain: &'a Domain) -> Self {
        Self {
            args: Vec::new(),
            count: 0,
            domain,
        }
    }

    /// Add an argument
    pub fn arg<S: Into<String>>(mut self, arg: S) -> Self {
        self.args.push(c_string_raw!(arg.into()));
        self.count += 1;
        self
    }

    /// Run the assembly
    pub fn run(mut self, assembly: &Assembly) -> i32 {
        self.args
            .insert(0, c_string_raw!(assembly.filename().to_string()));
        self.count += 1;

        self.domain
            .execute_assembly_with_args_raw(assembly, self.count, self.args.as_mut_ptr())
    }
}

impl Domain {
    /// # Parameters
    ///
    /// * name - file name of the assembly
    pub unsafe fn mono_domain_assembly_open(&self, name: *const c_char) {
        mono_sys::mono_domain_assembly_open(self.domain, name);
    }

    /// # Description
    ///
    /// Creates a new application domain, the unmanaged representation of the actual domain.
    /// Usually you will want to create the Application domains provide an isolation facility for
    /// assemblies. You can load assemblies and execute code in them that will not be visible to
    /// other application domains. This is a runtime-based virtualization technology.<br/>
    /// It is possible to unload domains, which unloads the assemblies and data that was allocated
    /// in that domain.<br/>
    /// When a domain is created a mempool is allocated for domain-specific structures, along a
    /// dedicated code manager to hold code that is associated with the domain. a
    ///
    /// # Return value
    ///
    /// New initialized MonoDomain, with no configuration or assemblies loaded into it.
    pub unsafe fn mono_domain_create() -> *mut mono_sys::MonoDomain {
        mono_sys::mono_domain_create()
    }

    /// # Description
    ///
    /// Returns a MonoDomain initialized with the appdomain.
    ///
    /// # Parameters
    ///
    /// * friendly_name - The friendly name of the appdomain to create
    /// * configuration_file - The configuration file to initialize the appdomain with
    pub unsafe fn mono_domain_create_appdomain(
        friendly_name: *mut c_char,
        configuration_file: *mut c_char,
    ) -> *mut mono_sys::MonoDomain {
        mono_sys::mono_domain_create_appdomain(friendly_name, configuration_file)
    }

    /// # Description
    ///
    /// Request finalization of all finalizable objects inside domain.
    /// Wait timeout msecs for the finalization to complete.
    ///
    /// # Return value
    ///
    /// `TRUE` if succeeded, `FALSE` if there was a timeout,
    ///
    /// # Parameters
    ///
    /// * timeout - msects to wait for the finalization to complete, -1 to wait indefinitely.
    pub unsafe fn mono_domain_finalize(&self, timeout: u32) -> mono_sys::mono_bool {
        mono_sys::mono_domain_finalize(self.domain, timeout)
    }

    /// # Description
    ///
    /// Use this method to safely iterate over all the loaded application domains in the current
    /// runtime. The provided func is invoked with a pointer to the MonoDomain and is given the
    /// value of the user_data parameter which can be used to pass state to your called routine.
    ///
    /// # Parameters
    ///
    /// * func - function to invoke with the domain data
    /// * user_data - user-defined pointer that is passed to the supplied func fo reach domain
    pub unsafe fn mono_domain_foreach(func: mono_sys::MonoDomainFunc, user_data: *mut c_void) {
        mono_sys::mono_domain_foreach(func, user_data);
    }

    /// # Description
    ///
    /// This releases the resources associated with the specific domain.
    /// This is a low-level function that is invoked by the AppDomain infrastructure when necessary.
    ///
    /// # Parameters
    ///
    /// * force - if true, it allows the root domain to be released (used at shutdown only).
    pub unsafe fn mono_domain_free(&self, force: mono_sys::mono_bool) {
        mono_sys::mono_domain_free(self.domain, force);
    }

    pub unsafe fn mono_domain_from_appdomain(
        appdomain: *mut mono_sys::MonoAppDomain,
    ) -> *mut mono_sys::MonoDomain {
        mono_sys::mono_domain_from_appdomain(appdomain)
    }

    pub unsafe fn mono_domain_get_by_id(domainid: c_int) -> *mut mono_sys::MonoDomain {
        mono_sys::mono_domain_get_by_id(domainid)
    }

    pub unsafe fn mono_domain_get_friendly_name(&self) -> *const c_char {
        mono_sys::mono_domain_get_friendly_name(self.domain)
    }

    pub unsafe fn mono_domain_get_id(&self) -> c_int {
        mono_sys::mono_domain_get_id(self.domain)
    }

    /// # Description
    ///
    /// This method returns the value of the current MonoDomain that this thread and code are
    /// running under. To obtain the root domain use mono_get_root_domain() API.
    ///
    /// # Return value
    ///
    /// the current domain
    pub unsafe fn mono_domain_get() -> *mut mono_sys::MonoDomain {
        mono_sys::mono_domain_get()
    }

    /// # Return value
    ///
    /// `TRUE` if the `AppDomain.TypeResolve` field has been set.
    pub unsafe fn mono_domain_has_type_resolve(&self) -> mono_sys::mono_bool {
        mono_sys::mono_domain_has_type_resolve(self.domain)
    }

    pub unsafe fn mono_domain_is_unloading(&self) -> mono_sys::mono_bool {
        mono_sys::mono_domain_is_unloading(self.domain)
    }

    /// # Description
    ///
    /// Returns whenever `VTABLE_SLOT` is inside a vtable which belongs to `DOMAIN`.
    pub unsafe fn mono_domain_owns_vtable_slot(
        &self,
        vtable_slot: *mut c_void,
    ) -> mono_sys::mono_bool {
        mono_sys::mono_domain_owns_vtable_slot(self.domain, vtable_slot)
    }

    /// # Description
    ///
    /// Used to set the system configuration for an appdomain.<br/>
    /// Without using this, embedded builds will get
    /// `System.Configuration.ConfigurationErrorsException: Error Initializing the configuration system. ---> System.ArgumentException: The 'ExeConfigFilename' argument cannot be null.`
    /// for some managed calls.
    ///
    /// # Parameters
    ///
    /// * base_dir - new base directory for the appdomain
    /// * config_file_name - path to the new configuration for the app domain
    pub unsafe fn mono_domain_set_config(
        &self,
        base_dir: *const c_char,
        config_file_name: *const c_char,
    ) {
        mono_sys::mono_domain_set_config(self.domain, base_dir, config_file_name);
    }

    /// # Description
    ///
    /// Sets the current domain to this domain.
    pub unsafe fn mono_domain_set_internal(&self) {
        mono_sys::mono_domain_set_internal(self.domain);
    }

    /// # Description
    ///
    /// Set the current appdomain to this domain. If force is set, set it even if it is being unloaded.
    ///
    /// # Return value
    ///
    /// `TRUE` on success; `FALSE` if the domain is unloaded.
    ///
    /// # Parameters
    ///
    /// * force - force setting.
    pub unsafe fn mono_domain_set(&self, force: mono_sys::mono_bool) -> mono_sys::mono_bool {
        mono_sys::mono_domain_set(self.domain, force)
    }

    /// # Description
    ///
    /// This routine invokes the internal `System.AppDomain.DoTypeResolve` and returns the
    /// assembly that matches name.<br/>
    /// If name is null, the value of ((TypeBuilder)tb).FullName is used instead.
    ///
    /// # Return value
    ///
    /// A `MonoReflectionAssembly` or `NULL` if not found.
    ///
    /// # Parameters
    ///
    /// * name - the name of the type to resolve or NULL.
    /// * tb - A System.Reflection.Emit.TypeBuilder, used if name is NULL.
    pub unsafe fn mono_domain_try_type_resolve(
        &self,
        name: *mut c_char,
        tb: *mut mono_sys::MonoObject,
    ) -> *mut mono_sys::MonoReflectionAssembly {
        mono_sys::mono_domain_try_type_resolve(self.domain, name, tb)
    }

    pub unsafe fn mono_domain_try_unload(&self, exc: *mut *mut mono_sys::MonoObject) {
        mono_sys::mono_domain_try_unload(self.domain, exc);
    }

    pub unsafe fn mono_domain_unload(&self) {
        mono_sys::mono_domain_unload(self.domain);
    }
}

/// # Contexts
impl Domain {
    /// # Description
    ///
    /// Initializes the domain's default System.Runtime.Remoting's Context.
    pub unsafe fn mono_context_init(&self) {
        mono_sys::mono_context_init(self.domain)
    }

    /// # Return value
    ///
    /// the current Mono Application Context.
    pub unsafe fn mono_context_get() -> *mut mono_sys::MonoAppContext {
        mono_sys::mono_context_get()
    }

    /// # Return value
    ///
    /// The ID of the domain that context was created in.
    pub unsafe fn mono_context_get_domain_id(&self) -> c_int {
        mono_sys::mono_context_get_domain_id(self.context)
    }

    /// # Description
    ///
    /// Context IDs are guaranteed to be unique for the duration of a Mono process;
    /// they are never reused.
    ///
    /// # Return value
    ///
    /// The unique ID for context.
    pub unsafe fn mono_context_get_id(&self) -> c_int {
        mono_sys::mono_context_get_id(self.context)
    }

    pub unsafe fn mono_context_set(&self, new_context: *mut mono_sys::MonoAppContext) {
        mono_sys::mono_context_set(new_context);
    }
}
