use crate::mono_sys;
use std::os::raw::{c_char, c_int, c_void};

#[derive(Debug, Clone)]
pub struct Image {
    image: *mut mono_sys::MonoImage,
}

impl Image {
    pub fn new(image: *mut mono_sys::MonoImage) -> Self {
        Self { image }
    }

    pub unsafe fn inner_image(&self) -> *mut mono_sys::MonoImage {
        self.image
    }
}

/// # Opening and closing MonoImages
impl Image {
    /// # Return value
    ///
    /// An open image of type `MonoImage` or `NULL` on error.
    /// The caller holds a temporary reference to the returned image which should be cleared
    /// when no longer needed by calling `mono_image_close()`. if `NULL`, then check the value of
    /// status for details on the error
    ///
    /// # Parameters
    ///
    /// * fname - filename that points to the module we want to open
    /// * status - An error condition is returned in this field
    pub unsafe fn mono_image_open(
        fname: *const c_char,
        status: *mut mono_sys::MonoImageOpenStatus,
    ) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_open(fname, status)
    }

    pub unsafe fn mono_image_open_full(
        fname: *const c_char,
        status: *mut mono_sys::MonoImageOpenStatus,
        refonly: mono_sys::mono_bool,
    ) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_open_full(fname, status, refonly)
    }

    pub unsafe fn mono_image_open_from_data(
        data: *mut c_char,
        data_len: u32,
        need_copy: mono_sys::mono_bool,
        status: *mut mono_sys::MonoImageOpenStatus,
    ) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_open_from_data(data, data_len, need_copy, status)
    }

    pub unsafe fn mono_image_open_from_data_full(
        data: *mut c_char,
        data_len: u32,
        need_copy: mono_sys::mono_bool,
        status: *mut mono_sys::MonoImageOpenStatus,
        refonly: mono_sys::mono_bool,
    ) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_open_from_data_full(data, data_len, need_copy, status, refonly)
    }

    pub unsafe fn mono_image_open_from_data_with_name(
        data: *mut c_char,
        data_len: u32,
        need_copy: mono_sys::mono_bool,
        status: *mut mono_sys::MonoImageOpenStatus,
        refonly: mono_sys::mono_bool,
        name: *const c_char,
    ) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_open_from_data_with_name(
            data, data_len, need_copy, status, refonly, name,
        )
    }

    /// # Description
    ///
    /// Closes an image file, deallocates all memory consumed and unmaps all
    /// possible sections of the file
    pub unsafe fn mono_image_close(&self) {
        mono_sys::mono_image_close(self.image);
    }

    /// # Description
    ///
    /// Increases the reference count of an image.
    pub unsafe fn mono_image_addref(&self) {
        mono_sys::mono_image_addref(self.image);
    }

    pub unsafe fn mono_image_load_file_for_image(
        &self,
        fileidx: c_int,
    ) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_load_file_for_image(self.image, fileidx)
    }

    /// # Description
    ///
    /// Load the module with the one-based index `IDX` from `IMAGE` and return it.
    /// Return `NULL` if it cannot be loaded.
    pub unsafe fn mono_image_load_module(&self, idx: c_int) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_load_module(self.image, idx)
    }
}

/// # Image Information
impl Image {
    pub unsafe fn mono_image_get_guid(&self) -> *const c_char {
        mono_sys::mono_image_get_guid(self.image)
    }

    /// # Description
    ///
    /// Use this routine to get the assembly that owns this image.
    ///
    /// # Return value
    ///
    /// the assembly that holds this image.
    pub unsafe fn mono_image_get_assembly(&self) -> *mut mono_sys::MonoAssembly {
        mono_sys::mono_image_get_assembly(self.image)
    }

    /// # Description
    ///
    /// Use this routine to determine the metadata token for method that has been
    /// flagged as the entry point.
    ///
    /// # Return value
    ///
    /// the token for the entry point method in the image
    pub unsafe fn mono_image_get_entry_point(&self) -> u32 {
        mono_sys::mono_image_get_entry_point(self.image)
    }

    /// # Description
    ///
    /// Used to get the filename that hold the actual MonoImage
    ///
    /// # Return value
    ///
    /// the filename.
    pub unsafe fn mono_image_get_filename(&self) -> *const c_char {
        mono_sys::mono_image_get_filename(self.image)
    }

    /// # Return value
    ///
    /// the name of the assembly.
    pub unsafe fn mono_image_get_name(image: *mut mono_sys::MonoImage) -> *const c_char {
        mono_sys::mono_image_get_name(image)
    }

    /// # Description
    ///
    /// This is a low-level routine that fetches a resource from the metadata that starts at a
    /// given offset. The size parameter is filled with the data field as encoded in the metadata
    ///
    /// # Return value
    ///
    /// the pointer to the resource whose offset is offset.
    ///
    /// # Parameters
    ///
    /// * offset - The offset to add to the resource
    /// * size - a pointer to an int where the size of the resource will be stored
    pub unsafe fn mono_image_get_resource(&self, offset: u32, size: *mut u32) -> *const c_char {
        mono_sys::mono_image_get_resource(self.image, offset, size)
    }

    pub unsafe fn mono_image_get_table_info(
        &self,
        table_id: c_int,
    ) -> *const mono_sys::MonoTableInfo {
        mono_sys::mono_image_get_table_info(self.image, table_id)
    }

    pub unsafe fn mono_image_get_table_rows(&self, table_id: c_int) -> c_int {
        mono_sys::mono_image_get_table_rows(self.image, table_id)
    }

    /// # Description
    ///
    /// Determines if the given image was created dynamically through the
    /// `System.Reflection.Emit` API
    ///
    /// # Return value
    ///
    /// `TRUE` if the image was created dynamically, `FALSE` if not.
    pub unsafe fn mono_image_is_dynamic(&self) -> mono_sys::mono_bool {
        mono_sys::mono_image_is_dynamic(self.image)
    }

    pub unsafe fn mono_image_loaded_by_guid(guid: *const c_char) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_loaded_by_guid(guid)
    }

    /// # Description
    ///
    /// This routine verifies that the given image is loaded. Reflection-only loads do not count.
    ///
    /// # Return value
    ///
    /// the loaded MonoImage, or NULL on failure.
    ///
    /// # Parameters
    ///
    /// * name - path or assembly name of the image to load
    pub unsafe fn mono_image_loaded(name: *const c_char) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_loaded(name)
    }

    /// # Return value
    ///
    /// `NULL` if not found, otherwise a pointer to the in-memory representation of the
    /// given resource. The caller should free it using `g_free()` when no longer needed.
    pub unsafe fn mono_image_lookup_resource(
        &self,
        res_id: u32,
        lang_id: u32,
        name: *mut mono_sys::mono_unichar2,
    ) -> *mut c_void {
        mono_sys::mono_image_lookup_resource(self.image, res_id, lang_id, name)
    }

    /// # Return value
    ///
    /// a string describing the error.
    ///
    /// # Parameters
    ///
    /// * status - an code indicating the result from a recent operation
    pub unsafe fn mono_image_strerror(status: mono_sys::MonoImageOpenStatus) -> *const c_char {
        mono_sys::mono_image_strerror(status)
    }

    /// # Description
    ///
    /// This is used when JITing the `constrained.` opcode.<br/>
    /// This returns two values: the constrained method, which has been inflated as the function
    /// return value; And the original CIL-stream method as declared in cil_method.
    /// The later is used for verification.
    pub unsafe fn mono_get_method_constrained(
        &self,
        token: u32,
        constrained_class: *mut mono_sys::MonoClass,
        context: *mut mono_sys::MonoGenericContext,
        cil_method: *mut *mut mono_sys::MonoMethod,
    ) -> *mut mono_sys::MonoMethod {
        mono_sys::mono_get_method_constrained(
            self.image,
            token,
            constrained_class,
            context,
            cil_method,
        )
    }
}

/// # Public Keys, Strong Names and Certificates
impl Image {
    /// # Description
    ///
    /// If the image has a strong name, and size is not `NULL`, the value pointed to by size will
    /// have the size of the strong name.
    ///
    /// # Return value
    ///
    /// the position within the image file where the strong name is stored.
    ///
    /// # Parameters
    ///
    /// * size - a guint32 pointer, or `NULL`.
    pub unsafe fn mono_image_strong_name_position(&self, size: *mut u32) -> u32 {
        mono_sys::mono_image_strong_name_position(self.image, size)
    }

    /// # Description
    ///
    /// This is used to obtain the public key in the image.<br/>
    /// If the image has a public key, and size is not NULL, the value pointed to by size will
    /// have the size of the public key.
    ///
    /// # Return value
    ///
    /// `NULL` if the image does not have a public key, or a pointer to the public key.
    ///
    /// # Parameters
    ///
    /// * size - a guint32 pointer, or `NULL`.
    pub unsafe fn mono_image_get_public_key(&self, size: *mut u32) -> *const c_char {
        mono_sys::mono_image_get_public_key(self.image, size)
    }

    /// # Description
    ///
    /// If the image has a strong name, and size is not NULL, the value pointed to
    /// by size will have the size of the strong name.
    ///
    /// # Return value
    ///
    /// `NULL` if the image does not have a strong name, or a pointer to the public key.
    ///
    /// # Parameters
    ///
    /// * size - a guint32 pointer, or `NULL`.
    pub unsafe fn mono_image_get_strong_name(&self, size: *mut u32) -> *const c_char {
        mono_sys::mono_image_get_strong_name(self.image, size)
    }

    /// # Description
    ///
    /// Use this routine to determine if the image has a Authenticode Certificate Table.
    ///
    /// # Return value
    ///
    /// `TRUE` if the image contains an authenticode entry in the PE directory.
    pub unsafe fn mono_image_has_authenticode_entry(&self) -> mono_sys::mono_bool {
        mono_sys::mono_image_has_authenticode_entry(self.image)
    }
}

/// # Low-level features
impl Image {
    pub unsafe fn mono_image_rva_map(&self, rva: u32) -> *mut c_char {
        mono_sys::mono_image_rva_map(self.image, rva)
    }

    /// # Description
    ///
    /// This routine makes sure that we have an in-memory copy
    /// of an image section (.text, .rsrc, .data).
    ///
    /// # Return value
    ///
    /// `TRUE` on success
    ///
    /// # Parameters
    ///
    /// * section - section number that we will load/map into memory
    pub unsafe fn mono_image_ensure_section_idx(&self, section: c_int) -> c_int {
        mono_sys::mono_image_ensure_section_idx(self.image, section)
    }

    /// # Description
    ///
    /// This routine makes sure that we have an in-memory copy
    /// of an image section (.text, .rsrc, .data).
    ///
    /// # Return value
    ///
    /// `TRUE` on success
    ///
    /// # Parameters
    ///
    /// * section - section name that we will load/map into memory
    pub unsafe fn mono_image_ensure_section(&self, section: *const c_char) -> c_int {
        mono_sys::mono_image_ensure_section(self.image, section)
    }

    pub unsafe fn mono_image_init(&self) {
        mono_sys::mono_image_init(self.image);
    }

    /// # Description
    ///
    /// Initialize the global variables used by this module.
    pub unsafe fn mono_images_init() {
        mono_sys::mono_images_init();
    }

    /// # Description
    ///
    /// Free all resources used by this module.
    pub unsafe fn mono_images_cleanup() {
        mono_sys::mono_images_cleanup();
    }

    pub unsafe fn mono_image_add_to_name_cache(
        &self,
        nspace: *const c_char,
        name: *const c_char,
        idx: u32,
    ) {
        mono_sys::mono_image_add_to_name_cache(self.image, nspace, name, idx);
    }

    pub unsafe fn mono_image_fixup_vtable(&self) {
        mono_sys::mono_image_fixup_vtable(self.image);
    }

    pub unsafe fn mono_image_loaded_by_guid_full(
        guid: *const c_char,
        refonly: mono_sys::mono_bool,
    ) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_loaded_by_guid_full(guid, refonly)
    }

    pub unsafe fn mono_image_loaded_full(
        name: *const c_char,
        refonly: mono_sys::mono_bool,
    ) -> *mut mono_sys::MonoImage {
        mono_sys::mono_image_loaded_full(name, refonly)
    }
}
