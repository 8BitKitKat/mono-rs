#![allow(non_snake_case)]

extern crate mono;
use mono::str_from_c_ptr;

/// Needed to link this lib with the binary, otherwise mono will not be able to find the functions
pub fn dummy() {}

#[no_mangle]
pub extern "C" fn RustFunction() {
    println!("Hello from Rust!");
    native_rust_func();
}

pub fn native_rust_func() {
    println!("Hello from native rust function!");
}

#[no_mangle]
pub extern "C" fn RustGreet(name: *const std::os::raw::c_char) {
    println!("Hello, {}", str_from_c_ptr!(name));
}

#[no_mangle]
pub extern "C" fn RustAnswer() -> std::os::raw::c_int {
    42
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct UnmanagedStruct {
    x: std::os::raw::c_int,
    y: std::os::raw::c_int,
}

#[no_mangle]
pub extern "C" fn PassByValue(value: UnmanagedStruct) {
    dbg!(value);
}

#[no_mangle]
pub extern "C" fn PassByPtrIn(ref_: *mut UnmanagedStruct) {
    dbg!(&ref_);
    unsafe {
        dbg!(*ref_);
    }
}
#[no_mangle]
pub extern "C" fn PassByPtrOut(ref_: *mut UnmanagedStruct) {
    dbg!(&ref_);
    unsafe {
        dbg!(*ref_);
    }
}
#[no_mangle]
pub extern "C" fn PassByPtrInOut(ref_: *mut UnmanagedStruct) {
    dbg!(&ref_);
    unsafe {
        dbg!(*ref_);
    }
}

#[no_mangle]
pub extern "C" fn ReturnByValue() -> UnmanagedStruct {
    UnmanagedStruct { x: 1, y: 2 }
}
#[no_mangle]
pub extern "C" fn ReturnByPtr() -> *mut UnmanagedStruct {
    &mut UnmanagedStruct { x: 1, y: 2 }
}

#[no_mangle]
pub extern "C" fn PassByDoubleIndirection(ref_: *mut *mut UnmanagedStruct) {
    dbg!(&ref_);
    unsafe { dbg!(*ref_) };
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct Vec2 {
    x: f32,
    y: f32,
}

impl Vec2 {
    #[no_mangle]
    pub extern "C" fn vec2_debug_print(&self) {
        dbg!(self);
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct Vec3 {
    x: f32,
    y: f32,
    z: f32,
}

impl Vec3 {
    #[no_mangle]
    pub extern "C" fn vec3_debug_print(&self) {
        dbg!(self);
    }
}

#[repr(C)]
#[derive(Debug, Clone)]
pub struct Entity {
    pos: Vec2,
}

impl Entity {
    #[no_mangle]
    pub extern "C" fn entity_debug_print(&self) {
        dbg!(self);
    }
}

#[repr(C)]
#[derive(Debug, Clone)]
pub struct Player {
    num: i32,
}

impl Player {
    #[no_mangle]
    pub extern "C" fn player_debug_print(&self) {
        dbg!(self);
    }
}
